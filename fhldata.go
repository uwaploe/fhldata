// Fhldata parses and stores data records from the FHLOO sensors.
package main

import (
	"bytes"
	"flag"
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/garyburd/redigo/redis"
	"github.com/influxdata/influxdb/client/v2"
	"github.com/pkg/errors"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: fhldata [options] CFGFILE

Read FHLOO data from multiple Redis pub-sub channels and write the
data records to an InfluxDB database.
`

type serverConfig struct {
	Host   string `toml:"host"`
	Port   int    `toml:"port"`
	Dbname string `toml:"dbname,omitempty"`
}

type sensorConfig struct {
	Name    string            `toml:"name"`
	Rdchan  string            `toml:"rawdata"`
	Sep     string            `toml:"sep,omitempty"`
	Rectype string            `toml:"rectype,omitempty"`
	Fields  []string          `toml:"fields,omitempty"`
	Tags    map[string]string `toml:"tags,omitempty"`
}

type sysConfig struct {
	Servers map[string]serverConfig `toml:"servers"`
	Sensors []sensorConfig          `toml:"sensors"`
}

// Raw record format is a series delimiter separated numbers.
func parse_std_record(rawdata []byte, sens sensorConfig) (*client.Point, error) {
	var err error
	rec := make(map[string]interface{})
	// The timestamp is separated from the raw data record by a single space.
	parts := bytes.SplitN(rawdata, []byte(" "), 2)
	ts, err := strconv.ParseInt(string(parts[0]), 10, 64)
	if err != nil {
		return nil, errors.Wrap(err, "timestamp parse failed")
	}

	for i, v := range bytes.Split(parts[1], []byte(sens.Sep)) {
		if i >= len(sens.Fields) {
			break
		}
		rec[sens.Fields[i]], err = strconv.ParseFloat(string(bytes.Trim(v, " \t")), 32)
		if err != nil {
			log.Printf("Error parsing: %s.%s (%v)\n",
				sens.Name, sens.Fields[i], err)
		}
	}

	return client.NewPoint(sens.Name, sens.Tags, rec, time.Unix(ts, 0))
}

// Raw data record is stored in a field named "content"
func parse_raw_record(rawdata []byte, sens sensorConfig) (*client.Point, error) {
	var err error
	rec := make(map[string]interface{})
	// The timestamp is separated from the raw data record by a single space.
	parts := bytes.SplitN(rawdata, []byte(" "), 2)
	ts, err := strconv.ParseInt(string(parts[0]), 10, 64)
	if err != nil {
		return nil, errors.Wrap(err, "timestamp parse failed")
	}
	rec["content"] = string(parts[1])
	return client.NewPoint(sens.Name, sens.Tags, rec, time.Unix(ts, 0))
}

func data_reader(psc redis.PubSubConn, sensmap map[string]sensorConfig) <-chan *client.Point {
	c := make(chan *client.Point, len(sensmap))
	go func() {
		defer close(c)
		var (
			pt  *client.Point
			err error
		)
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				}
			case redis.Message:
				if sens, found := sensmap[msg.Channel]; found {
					switch sens.Rectype {
					case "raw":
						pt, err = parse_raw_record(msg.Data, sens)
					default:
						pt, err = parse_std_record(msg.Data, sens)
					}
					if err == nil {
						c <- pt
					} else {
						log.Println(err)
					}
				}
			}
		}
	}()

	return c
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	showvers := flag.Bool("version", false,
		"Show program version information and exit")
	user := flag.String("user", "", "Database user name")
	pword := flag.String("pword", "", "Database password")

	flag.Parse()
	if *showvers {
		fmt.Printf("%s %s\n", os.Args[0], Version)
		fmt.Printf("  Build date: %s\n", BuildDate)
		fmt.Printf("  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	var syscfg sysConfig
	if _, err := toml.DecodeFile(args[0], &syscfg); err != nil {
		log.Fatal(err)
	}

	// One Redis connection for pub-sub
	psconn, err := redis.Dial("tcp",
		fmt.Sprintf("%s:%d", syscfg.Servers["redis"].Host, syscfg.Servers["redis"].Port))
	if err != nil {
		log.Fatalln(err)
	}
	psc := redis.PubSubConn{Conn: psconn}

	// And a second connection for data publishing
	conn, err := redis.Dial("tcp",
		fmt.Sprintf("%s:%d", syscfg.Servers["redis"].Host, syscfg.Servers["redis"].Port))
	if err != nil {
		log.Fatalln(err)
	}

	// Configure the HTTP connection to InfluxDB
	cfg := client.HTTPConfig{
		Addr: fmt.Sprintf("http://%s:%d",
			syscfg.Servers["influxdb"].Host, syscfg.Servers["influxdb"].Port),
	}

	if *user != "" {
		cfg.Username = *user
		cfg.Password = *pword
	}

	cln, err := client.NewHTTPClient(cfg)
	if err != nil {
		log.Fatalf("Error creating InfluxDB Client: %v", err.Error())
	}
	defer cln.Close()

	sensmap := make(map[string]sensorConfig)
	for _, s := range syscfg.Sensors {
		sensmap[s.Rdchan] = s
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v. Exiting.", s)
			psc.Unsubscribe()
		}
	}()

	// Start the data reader goroutine
	c := data_reader(psc, sensmap)
	// Subscribe to the sensor raw-data channels
	for _, s := range syscfg.Sensors {
		psc.Subscribe(s.Rdchan)
	}

	for pt := range c {
		// Publish to a Redis channel ...
		conn.Do("PUBLISH", "data."+pt.Name(), pt.PrecisionString("s"))
		// ... and add to the database.
		bp, _ := client.NewBatchPoints(client.BatchPointsConfig{
			Database:  syscfg.Servers["influxdb"].Dbname,
			Precision: "s",
		})
		bp.AddPoint(pt)
		err = cln.Write(bp)
		if err != nil {
			log.Println(err)
		}
	}
}
