# FHLOO Data Manager

## Summary

The FHLOO Data Manager subscribes to [Redis](http://redis.io) pub-sub
channels and reads the sensor raw-data messages published by the Sensor
Drivers. The raw records are parsed and stored in
an [InfluxDB time-series database](https://www.influxdata.com). The parsed
records are also published to a Redis channel named *data.SENSORNAME*.

## Usage

``` shellsession
$ fhldata --help

Usage: fhldata [options] CFGFILE

Read FHLOO data from multiple Redis pub-sub channels and write the
data records to an InfluxDB database.
  -pword string
    	Database password
  -user string
    	Database user name
  -version
    	Show program version information and exit
```

## Configuration File

The configuration file uses the [TOML](https://en.wikipedia.org/wiki/TOML)
format.

``` toml
[servers]
  [servers.redis]
  host = "localhost"
  port = 6379

  [servers.influxdb]
  host = "localhost"
  port = 8086
  dbname = "fhloo"

[[sensors]]
name = "ctd"
# Channel for raw data records
rawdata = "rawdata.ctd.1"
# Field separator in the raw data record
sep = ","
# Data field names
fields = ["tempwat", "condwat", "preswat"]
# Tags to add to each database record
tags = {"index" = "1", "model" = "37SMP"}

```
