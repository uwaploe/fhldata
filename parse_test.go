package main

import (
	"io/ioutil"
	"math"
	"path/filepath"
	"testing"
)

func helperLoadBytes(t *testing.T, name string) []byte {
	path := filepath.Join("testdata", name)
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatal(err)
	}
	return bytes
}

func relerr(x, y float64) float64 {
	return math.Abs(y-x) / y
}

// Test parsing Seabird CTD data records.
func TestSbeCtd(t *testing.T) {
	// Timestamp is Aug  3 17:25:07 UTC 2017
	rawdata := []byte("1501781107   25.6935,  0.00689,    0.026, 03 Aug 2017, 09:24:53\r")
	sc := sensorConfig{
		Name:   "ctd",
		Sep:    ",",
		Fields: []string{"tempwat", "condwat", "preswat"},
		Tags:   map[string]string{"index": "1"},
	}

	pt, err := parse_std_record(rawdata, sc)
	if err != nil {
		t.Error(err)
	}

	h, m, s := pt.Time().UTC().Clock()
	if h != 17 || m != 25 || s != 7 {
		t.Errorf("Timestamp decode error: %v", pt.Time())
	}

	fields, err := pt.Fields()
	if err != nil {
		t.Error(err)
	}

	x, ok := fields["tempwat"].(float64)
	if !ok {
		t.Errorf("Wrong data type: %T != float64", x)
	}

	if relerr(x, 25.6935) > 0.000001 {
		t.Errorf("Inaccurate value: %v", x)
	}
}

// Test parsing ADCP records
func TestAdcp(t *testing.T) {
	// Timestamp is Aug  3 17:25:07 UTC 2017
	rawdata := helperLoadBytes(t, "adcp.hex")
	sc := sensorConfig{
		Name: "adcp",
		Tags: map[string]string{"index": "1"},
	}

	pt, err := parse_raw_record(rawdata, sc)
	if err != nil {
		t.Error(err)
	}

	h, m, s := pt.Time().UTC().Clock()
	if h != 17 || m != 25 || s != 7 {
		t.Errorf("Timestamp decode error: %v", pt.Time())
	}

	fields, err := pt.Fields()
	if err != nil {
		t.Error(err)
	}

	x, ok := fields["content"].(string)
	if !ok {
		t.Errorf("Wrong data type: %T != string", x)
	}

	if len(x) != 3508 {
		t.Errorf("Bad record length: %d != 3508", len(x))
	}

}
