FROM alpine:latest

COPY build/fhldata-*-linux-amd64 /usr/bin/fhldata

ENTRYPOINT ["fhldata"]
