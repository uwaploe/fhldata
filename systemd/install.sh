#!/usr/bin/env bash
#
# Install the fhldata systemd service script under a user account.
#

# Are persistant services allowed for this user?
linger="$(loginctl show-user $USER | sed -n -e '/^Linger/s/Linger=//p')"
[[ "$linger" = "yes" ]] || {
    echo "You must allow long running services for user $USER ..."
    echo " sudo loginctl enable-linger $USER"
    exit 1
}

prog="$(which fhldata)"
[[ -n "$prog" ]] || {
    echo "fhldata is not installed"
    exit 1
}

BINDIR="${prog%/*}"
SVCDIR=$HOME/.config/systemd/user
CFGDIR=$HOME/.config/fhloo

echo "Creating installation directories ..."
mkdir -p $SVCDIR $CFGDIR

default="../data.toml"
read -e -i "$default" -p "Enter fhldata configuration file path: [$default] " ans
CFGFILE="${CFGDIR}/${ans##*/}"
if [[ -s "$ans" ]]; then
    cp -v "$ans" $CFGDIR
else
    echo "WARNING: configuration file '$ans' not found"
    exit 1
fi

read -e -i "$USER" -p "Enter database user name: [$USER] " DBUSER
read -e -p "Enter database password: " DBPASS

# Create the service file from the template
sed -e "s!@HOME@!$HOME!g" \
    -e "s!@BINDIR@!$BINDIR!g" \
    -e "s!@CFGFILE@!$CFGFILE!g" \
    -e "s!@USER@!$DBUSER!g" \
    -e "s!@PASSWORD@!$DBPASS!g" fhldata.service.in > $SVCDIR/fhldata.service

echo "You can now start the service"
echo "  systemctl --user start fhldata.service"
echo "To enable the service after future reboots, run the command below"
echo "  systemctl --user enable fhldata.service"
