#
DATE    ?= $(shell date +%FT%T%z)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
			cat $(CURDIR)/.version 2> /dev/null || echo v0)

PROG = fhldata
SRCS = $(wildcard *.go)

build = mkdir -p build && CGO_ENABLED=0 GOOS=$(1) GOARCH=$(2) go build \
   -o build/$(PROG)-$(3)-$(1)-$(2)$(4) \
   -tags release \
   -ldflags '-X main.Version=$(VERSION) -X main.BuildDate=$(DATE)'


.PHONY: clean linux darwin windows

all: linux darwin

clean:
	rm -rf build

linux: build/$(PROG)-$(VERSION)-linux-amd64

build/$(PROG)-$(VERSION)-linux-amd64: $(SRCS)
	$(call build,linux,amd64,$(VERSION))

darwin: build/$(PROG)-$(VERSION)-darwin-amd64

build/$(PROG)-$(VERSION)-darwin-amd64: $(SRCS)
	$(call build,darwin,amd64,$(VERSION))

windows: build/$(PROG)-$(VERSION)-windows-amd64.exe

build/$(PROG)-$(VERSION)-windows-amd64.exe: $(SRCS)
	$(call build,windows,amd64,$(VERSION),.exe)

dist: linux darwin
	cd build && upload-to-bitbucket.sh uwaploe $(PROG)-$(VERSION)-*

image: linux
	docker build -t fhldata:$(VERSION) -t fhldata:latest .